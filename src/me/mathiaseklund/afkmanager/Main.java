package me.mathiaseklund.afkmanager;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.afkmanager.commands.AFKCommand;
import me.mathiaseklund.afkmanager.listeners.EventListener;
import me.mathiaseklund.afkmanager.managers.AFKManager;
import me.mathiaseklund.afkmanager.utility.Messages;
import me.mathiaseklund.afkmanager.utility.Util;

public class Main extends JavaPlugin {

	static Main main;

	AFKManager manager;
	Messages messages;
	Util util;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		loadDependencies();
		loadFiles();
		loadManagers();
		loadListeners();
		loadCommands();
	}

	public void onDisable() {

	}

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		util = new Util();

		f = new File(getDataFolder(), "messages.yml");
		if (!f.exists()) {
			saveResource("messages.yml", true);
		}
		messages = new Messages(f);
	}

	void loadListeners() {
		Bukkit.getPluginManager().registerEvents(new EventListener(), this);
	}

	void loadCommands() {
		getCommand("afk").setExecutor(new AFKCommand());
	}

	void loadDependencies() {

	}

	void loadManagers() {
		manager = new AFKManager();
	}

	public AFKManager getAFKManager() {
		return manager;
	}

	public Messages getMsg() {
		return messages;
	}

	public Util getUtil() {
		return util;
	}
}
