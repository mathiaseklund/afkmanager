package me.mathiaseklund.afkmanager.api;

import org.bukkit.entity.Player;

import me.mathiaseklund.afkmanager.Main;
import me.mathiaseklund.afkmanager.managers.AFKManager;

public class AFKAPI {

	public static Main getMain() {
		return Main.getMain();
	}

	public static AFKManager getAFKManager() {
		return Main.getMain().getAFKManager();
	}

	public static boolean isAfk(Player player) {
		return Main.getMain().getAFKManager().isAfk(player);
	}
}
