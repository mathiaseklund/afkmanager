package me.mathiaseklund.afkmanager.commands;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.afkmanager.Main;
import me.mathiaseklund.afkmanager.managers.AFKManager;
import me.mathiaseklund.afkmanager.utility.Util;

public class AFKCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();
	AFKManager manager = main.getAFKManager();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 0) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("afkmanager.toggle.self")) {
					manager.updateLastAction(player,
							Calendar.getInstance().getTimeInMillis() - manager.getAfkAfter() * 1000);
					manager.toggleAfk(player);
				} else {
					// no perm
					for (String s : main.getMsg().getMessage("command-no-perm")) {
						util.message(player, s);
					}
				}
			} else {
				// console command. /afk
				for (String s : main.getMsg().getMessage("admin-command-list")) {
					util.message(sender, s);
				}
			}
		} else {
			if (args.length >= 1) {
				if (sender.hasPermission("afkmanager.admin")) {
					Player target = Bukkit.getPlayer(args[0]);
					if (target != null) {
						manager.toggleAfk(sender, target);
					} else {
						// no target found.
						if (args[0].equalsIgnoreCase("help")) {
							for (String s : main.getMsg().getMessage("admin-command-list")) {
								util.message(sender, s);
							}
						} else if (args[0].equalsIgnoreCase("info")) {
							if (args.length < 2) {
								util.message(sender, "&e/afk info <player> &7-&f Displays a players AFK Status.");
							} else {
								target = Bukkit.getPlayer(args[1]);
								if (target != null) {
									manager.displayAfkStatus(sender, target);
								} else {
									for (String s : main.getMsg().getMessage("command-target-not-found")) {
										util.message(sender, s);
									}
								}
							}
						} else {
							for (String s : main.getMsg().getMessage("command-target-not-found")) {
								util.message(sender, s);
							}
						}
					}
				} else {
					// no perm
					for (String s : main.getMsg().getMessage("command-no-perm")) {
						util.message(sender, s);
					}
				}
			}
		}
		return false;
	}

}
