package me.mathiaseklund.afkmanager.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AfkToggledEvent extends Event implements Cancellable {
	static final HandlerList handlers = new HandlerList();

	private Player player;
	private boolean result;
	private boolean isCancelled;

	public AfkToggledEvent(Player p, boolean result) {
		this.player = p;
		this.result = result;
		this.isCancelled = false;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean getResult() {
		return result;
	}
	
	public boolean isCancelled() {
		return isCancelled;
	}
	
	public void setCancelled(boolean b) {
		isCancelled = b;
	}
}
