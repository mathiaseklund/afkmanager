package me.mathiaseklund.afkmanager.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mathiaseklund.afkmanager.Main;
import me.mathiaseklund.afkmanager.managers.AFKManager;
import me.mathiaseklund.afkmanager.utility.Util;

public class EventListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();
	AFKManager manager = main.getAFKManager();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				manager.updateLastAction(event.getPlayer());
			}
		});
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		String uuid = event.getPlayer().getUniqueId().toString();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				manager.playerQuit(uuid);
			}
		});
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			util.debug(event.getMessage());
			if (!event.getMessage().contains("afk")) {
				if (manager.isCommandEnabled()) {
					Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
						public void run() {
							manager.updateLastAction(player);
							Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
								public void run() {
									if (manager.isAfk(player)) {
										manager.toggleAfk(player);
									}
								}
							}, 5);
						}
					});
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerMove(PlayerMoveEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			if (manager.isMoveEnabled()) {
				Location loc = event.getFrom();
				Block b = loc.getBlock();
				boolean cont = true;
				if (b.getType() == Material.LAVA || b.getType() == Material.WATER) {
					if (!manager.isMoveInLiquidEnabled()) {
						cont = false;
						// do not count.
					}
				} else {
					Block block = b.getRelative(BlockFace.NORTH);
					if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
						if (!manager.isMoveInLiquidEnabled()) {
							cont = false;
							// do not count.
						}
					}
					if (cont) {
						block = b.getRelative(BlockFace.EAST);
						if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
							if (!manager.isMoveInLiquidEnabled()) {
								cont = false;
								// do not count.
							}
						}
						if (cont) {
							block = b.getRelative(BlockFace.SOUTH);
							if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
								if (!manager.isMoveInLiquidEnabled()) {
									cont = false;
									// do not count.
								}
							}
							if (cont) {
								block = b.getRelative(BlockFace.WEST);
								if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
									if (!manager.isMoveInLiquidEnabled()) {
										cont = false;
										// do not count.
									}
								}
								if (cont) {
									block = b.getRelative(BlockFace.UP);
									if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
										if (!manager.isMoveInLiquidEnabled()) {
											cont = false;
											// do not count.
										}
									}
									if (cont) {
										block = b.getRelative(BlockFace.DOWN);
										if (block.getType() == Material.LAVA || block.getType() == Material.WATER) {
											if (!manager.isMoveInLiquidEnabled()) {
												cont = false;
												// do not count.
											}
										}
									}
								}
							}
						}
					}
				}
				if (cont) {
					Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
						public void run() {
							manager.updateLastAction(player);
							Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
								public void run() {
									if (manager.isAfk(player)) {
										manager.toggleAfk(player);
									}
								}
							}, 5);
						}
					});
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (manager.isInteractEnabled()) {
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					manager.updateLastAction(player);
					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							if (manager.isAfk(player)) {
								manager.toggleAfk(player);
							}
						}
					}, 5);
				}
			});
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if (!event.isCancelled()) {
			if (manager.isChatEnabled()) {
				Player player = event.getPlayer();
				Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
					public void run() {
						manager.updateLastAction(player);
						Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
							public void run() {
								if (manager.isAfk(player)) {
									manager.toggleAfk(player);
								}
							}
						}, 5);
					}
				});
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryOpen(InventoryOpenEvent event) {
		Player player = (Player) event.getPlayer();
		if (manager.isOpenInventoryEnabled()) {
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					manager.updateLastAction(player);
					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							if (manager.isAfk(player)) {
								manager.toggleAfk(player);
							}
						}
					}, 5);
				}
			});
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		if (manager.isCloseInventoryEnabled()) {
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					manager.updateLastAction(player);
					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							if (manager.isAfk(player)) {
								manager.toggleAfk(player);
							}
						}
					}, 5);
				}
			});
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (manager.isClickInventoryEnabled()) {
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					manager.updateLastAction(player);
					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							if (manager.isAfk(player)) {
								manager.toggleAfk(player);
							}
						}
					}, 5);
				}
			});
		}
	}
}
