package me.mathiaseklund.afkmanager.managers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.afkmanager.Main;
import me.mathiaseklund.afkmanager.events.AfkToggledEvent;
import me.mathiaseklund.afkmanager.utility.Util;
import net.md_5.bungee.api.ChatColor;

public class AFKManager {

	Main main;
	Util util;

	HashMap<String, Long> lastAction = new HashMap<String, Long>();
	ArrayList<String> afk = new ArrayList<String>();

	int afk_after = 30, kick_after = 30;
	String afk_kick_msg;
	boolean move, join, chat, interact, command, open_inventory;
	boolean close_inventory, click_inventory, kick_afk, move_inliquid;

	public AFKManager() {
		main = Main.getMain();
		util = main.getUtil();
		loadData();

		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				processLastActionList();
			}
		}, 20);
	}

	void loadData() {
		afk_after = main.getConfig().getInt("go-afk-after", 30);
		move = main.getConfig().getBoolean("afk-actions.move.enabled", true);
		join = main.getConfig().getBoolean("afk-actions.join.enabled", true);
		chat = main.getConfig().getBoolean("afk-actions.chat.enabled", true);
		interact = main.getConfig().getBoolean("afk-actions.interact.enabled", true);
		command = main.getConfig().getBoolean("afk-actions.command.enabled", true);
		open_inventory = main.getConfig().getBoolean("afk-actions.inventory.open", true);
		close_inventory = main.getConfig().getBoolean("afk-actions.inventory.close", true);
		click_inventory = main.getConfig().getBoolean("afk-actions.inventory.click", true);
		kick_afk = main.getConfig().getBoolean("kick-afk.enabled", true);
		kick_after = main.getConfig().getInt("kick-afk.after", 30);
		afk_kick_msg = main.getMsg().getFile().getString("player-afk-kicked");
		move_inliquid = main.getConfig().getBoolean("afk-actions.move.inliquid", true);
	}

	public boolean isAfk(String uuid) {
		return afk.contains(uuid);
	}

	public boolean isAfk(Player player) {
		return isAfk(player.getUniqueId().toString());
	}

	public void updateLastAction(String uuid) {
		lastAction.put(uuid, Calendar.getInstance().getTimeInMillis());
	}

	public void updateLastAction(String uuid, long l) {
		lastAction.put(uuid, l);
	}

	public void updateLastAction(Player player) {
		updateLastAction(player.getUniqueId().toString());
	}

	public void updateLastAction(Player player, long l) {
		updateLastAction(player.getUniqueId().toString(), l);
	}

	public void toggleAfk(Player player) {
		Bukkit.getScheduler().runTask(main, new Runnable() {
			public void run() {
				String uuid = player.getUniqueId().toString();
				if (isAfk(uuid)) {
					afk.remove(uuid);
					for (String s : main.getMsg().getMessage("player-afk-disabled")) {
						util.message(player, s);
					}
					lastAction.put(uuid, Calendar.getInstance().getTimeInMillis());

					// TODO add custom afk toggled event.
				} else {
					afk.add(player.getUniqueId().toString());
					for (String s : main.getMsg().getMessage("player-afk-enabled")) {
						util.message(player, s);
					}
					lastAction.put(uuid, Calendar.getInstance().getTimeInMillis() - (afk_after * 1000));

					// TODO add custom afk toggled event.
				}
			}
		});

	}

	public void toggleAfk(String uuid) {
		Bukkit.getScheduler().runTask(main, new Runnable() {
			public void run() {
				Player player = Bukkit.getPlayer(UUID.fromString(uuid));
				if (player != null) {
					if (isAfk(uuid)) {
						AfkToggledEvent event = new AfkToggledEvent(player, false);
						Bukkit.getPluginManager().callEvent(event);
						if (!event.isCancelled()) {
							afk.remove(uuid);
							lastAction.put(uuid, Calendar.getInstance().getTimeInMillis());
							for (String s : main.getMsg().getMessage("player-afk-disabled")) {
								util.message(player, s);
							}
						} else {
							util.debug("Afk toggle cancelled.");
						}

						// TODO add custom afk toggled event.
					} else {
						AfkToggledEvent event = new AfkToggledEvent(player, true);
						Bukkit.getPluginManager().callEvent(event);
						if (!event.isCancelled()) {
							afk.add(player.getUniqueId().toString());
							lastAction.put(uuid, Calendar.getInstance().getTimeInMillis() - (afk_after * 1000));
							for (String s : main.getMsg().getMessage("player-afk-enabled")) {
								util.message(player, s);
							}
						} else {
							util.debug("Afk toggle cancelled.");
						}

						// TODO add custom afk toggled event.
					}
				} else {
					util.debug("Unable to toggle offline players AFK status.");
				}
			}
		});

	}

	public void toggleAfk(CommandSender sender, Player target) {
		Bukkit.getScheduler().runTask(main, new Runnable() {
			public void run() {
				if (target != null) {
					String uuid = target.getUniqueId().toString();
					if (isAfk(uuid)) {
						AfkToggledEvent event = new AfkToggledEvent(target, false);
						Bukkit.getPluginManager().callEvent(event);
						if (!event.isCancelled()) {
							afk.remove(uuid);
							lastAction.put(uuid, Calendar.getInstance().getTimeInMillis());
							for (String s : main.getMsg().getMessage("player-afk-disabled")) {
								util.message(target, s);
							}
							for (String s : main.getMsg().getMessage("player-afk-disabled-other")) {
								util.message(sender, s.replaceAll("%target%", target.getName()));
							}
						} else {
							util.debug("Afk toggle cancelled.");
							for (String s : main.getMsg().getMessage("player-afk-disabled-other-interrupted")) {
								util.message(sender, s);
							}
						}
					} else {
						AfkToggledEvent event = new AfkToggledEvent(target, true);
						Bukkit.getPluginManager().callEvent(event);
						if (!event.isCancelled()) {
							afk.add(uuid);
							lastAction.put(uuid, Calendar.getInstance().getTimeInMillis() - (afk_after * 1000));
							for (String s : main.getMsg().getMessage("player-afk-enabled")) {
								util.message(target, s);
							}
							for (String s : main.getMsg().getMessage("player-afk-enabled-other")) {
								util.message(sender, s.replaceAll("%target%", target.getName()));
							}
						} else {
							util.debug("Afk toggle cancelled.");
							for (String s : main.getMsg().getMessage("player-afk-disabled-other-interrupted")) {
								util.message(sender, s);
							}
						}
					}
				} else {
					util.debug("Unable to toggle offline players AFK status.");
				}
			}
		});

	}

	void processLastActionList() {
		HashMap<String, Long> map = lastAction;
		long now = Calendar.getInstance().getTimeInMillis();
		for (String s : map.keySet()) {
			long l = map.get(s);

			long diff = now - l;
			// util.debug("Difference: " + diff);

			if (diff >= (afk_after * 1000)) {
				if (isAfk(s)) {
					if (getKickAfk()) {
						if (diff >= (afk_after * 1000) + (kick_after * 1000)) {
							Player player = Bukkit.getPlayer(UUID.fromString(s));
							if (player != null) {
								player.kickPlayer(ChatColor.translateAlternateColorCodes('&', afk_kick_msg));
							} else {
								util.debug("Unable to kick non-existant player.");
							}
						}
					}
					// do nothing.
				} else {
					Bukkit.getScheduler().runTask(main, new Runnable() {
						public void run() {
							toggleAfk(s);
						}
					});

				}
			}
		}
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				processLastActionList();
			}
		}, 20);
	}

	public void playerQuit(String uuid) {
		lastAction.remove(uuid);
		if (isAfk(uuid)) {
			afk.remove(uuid);
		}
	}

	public boolean isMoveEnabled() {
		return move;
	}

	public boolean isCommandEnabled() {
		return command;
	}

	public boolean isJoinEnabled() {
		return join;
	}

	public boolean isChatEnabled() {
		return chat;
	}

	public boolean isInteractEnabled() {
		return interact;
	}

	public boolean isCloseInventoryEnabled() {
		return close_inventory;
	}

	public boolean isOpenInventoryEnabled() {
		return open_inventory;
	}

	public boolean isClickInventoryEnabled() {
		return click_inventory;
	}

	public boolean getKickAfk() {
		return kick_afk;
	}

	public boolean isMoveInLiquidEnabled() {
		return move_inliquid;
	}

	public int getAfkAfter() {
		return afk_after;
	}

	public void displayAfkStatus(CommandSender sender, Player target) {
		//TODO
	}
}
