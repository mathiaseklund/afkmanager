package me.mathiaseklund.afkmanager.utility;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mathiaseklund.afkmanager.Main;

public class Messages {

	Main main;

	File f;
	FileConfiguration data;

	public Messages(File f) {
		main = Main.getMain();
		this.f = f;
		data = YamlConfiguration.loadConfiguration(f);
	}

	// GETTERS

	// public String getMessage(String path) {
	// return data.getString(path);
	// }

	public List<String> getMessage(String path) {
		return data.getStringList(path);
	}

	public FileConfiguration getFile() {
		return data;
	}
}
