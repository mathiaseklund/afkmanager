package me.mathiaseklund.afkmanager.utility;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.afkmanager.Main;

public class Util {

	Main main;
	public boolean debug = true;

	public Util() {
		main = Main.getMain();
		loadData();
	}

	void loadData() {
		debug = main.getConfig().getBoolean("debug", true);
	}

	public void debug(String message) {
		if (debug) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&eAFKManager DEBUG&7]&f " + message));
		}
	}

	public void message(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			} else {
				debug("Can't send a empty message.");
			}
		} else {
			debug("Can't send message to non-existent recipient.");
		}
	}
}
